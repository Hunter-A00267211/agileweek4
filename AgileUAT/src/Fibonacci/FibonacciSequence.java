package Fibonacci;

import java.util.Scanner;

public class FibonacciSequence {
	private Scanner sc=new Scanner(System.in);
	private int n;
	
	public int calculateNthValue(int fib){
		if(fib<=2){
			return 1;
		}
		else{
			return calculateNthValue(fib-1)+calculateNthValue(fib-2);
		}
	}
	
	public void validateN(){
		try{
			System.out.println("Please input n: ");
			n=sc.nextInt();
			if(n<=0){
				System.err.println("Number not in valid range!!!");
				return ;
			}
			System.out.println("f("+n+"): "+calculateNthValue(n));
		}catch(Exception e){
			System.err.println("Invalid number!!!");
		}
	}
	
	public static void main(String[] args) {
		FibonacciSequence fib=new FibonacciSequence();
		fib.validateN();
	}

}
